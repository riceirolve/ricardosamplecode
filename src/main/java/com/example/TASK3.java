package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

	public int distinct() {
		List<String> words = new ArrayList<String>();
		List<String> distincts = new ArrayList<String>();

		Random random = new Random();

		for (int i = 0; i < random.nextInt(100) + 1; i++)
			words.add(String.valueOf(((char) (65 + random.nextInt(24)))));

		for (int i = 0; i < words.size(); i++)
			if (!distincts.contains(words.get(i)))
				distincts.add(words.get(i));

		return distincts.size();
	}

}
