package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {
	public static void main(String[] args) {
		
		
		//What do you mean with "know the element before/after?
		List<String> list = new ArrayList<String>();
		Random random = new Random();
		
		for (int i = 0; i < random.nextInt(100) + 1; i++) 
			list.add(String.valueOf(((char) (65 + random.nextInt(24)))));
		
		System.out.println("List " + list);
		list.remove(Math.round(list.size() /2));
		System.out.println("List " + list);
	}
}
